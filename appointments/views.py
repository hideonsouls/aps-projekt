from datetime import date, timedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from .forms import DoctorForm, PatientForm, SpecialtyForm, DateForm, DateForm2, StartAppointForm, DoctorUpd #, AuthForm,
from .models import User, Doctor, Patient, Appointment, Schedule, Queue
# Create your views here.

def home(request):
    if request.user.is_authenticated:
        if request.user.grp == 3:
            appointments = request.user.patient.get_user_appointments()
            return render(request, 'home_usr.html', {'appointments': appointments})
        if request.user.grp == 2:
            schedules = []
            day = date.today()
            inc = timedelta(days=1)
            for i in range(0, 7):
                schedules.append(request.user.doctor.get_schedule(day))
                day += inc

            return render(request, 'home_doc.html', {'schedules': schedules, 'today': date.today(), 'type': 1})
        if request.user.grp == 1:
            doctors = Doctor.objects.all()
            return render(request, 'home_sec.html', {'doctors': doctors})
    else:
        return render(request, 'home.html')

@login_required
def schedule_appointments(request, pkey):
    schedule = get_object_or_404(Schedule, pk=pkey)
    appointments = schedule.get_appoints()

    return render(request, 'schedule_appointments.html', {'schedule': schedule, 'appointments': appointments})

def reg_doctor(request):
    if request.method == 'POST':
        form = DoctorForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.grp = 2 # Usuário sendo registrado é Doctor
            user.save()

            Doctor.objects.create(
                user=user,
                cpf=form.cleaned_data.get('cpf'),
                specialty=form.cleaned_data.get('specialty')
            )

            return redirect('home')

    else:
        form = DoctorForm()

    return render(request, 'signup.html', {'form': form})

def upd_doctor(request, doc_pk):
    doctor = get_object_or_404(Doctor, pk=doc_pk)
    if request.method == 'POST':
        form = DoctorUpd(request.POST, instance=doctor.user)
        if form.is_valid():
            user = form.save()

            doctor.user = user
            doctor.cpf = form.cleaned_data.get('cpf')
            doctor.specialty = form.cleaned_data.get('specialty')
            doctor.save()

            return redirect('home')

    else:
        # form = DoctorUpd(instance=doctor)
        form = DoctorUpd(initial={
            'username': doctor.user.username,
            'first_name': doctor.user.first_name ,
            'last_name': doctor.user.last_name,
            'email': doctor.user.email,
            'cpf': doctor.cpf,
            'specialty': doctor.specialty
        })

    return render(request, 'signup.html', {'form': form, 'doctor_pk': doc_pk})

def del_doctor(request, doc_pk):
    doctor = get_object_or_404(Doctor, pk=doc_pk)
    if doctor.total_appoints() > 0:
        return redirect('home')

    doctor.user.delete()
    return redirect('home')


def reg_patient(request):
    if request.method == 'POST':
        form = PatientForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.grp = 3  # Usuário sendo registrado é Patient
            user.save()

            year = form.cleaned_data.get('year')
            month = form.cleaned_data.get('month')
            day = form.cleaned_data.get('day')

            birth = date(year=year, month=month, day=day)

            Patient.objects.create(
                user=user,
                cpf=form.cleaned_data.get('cpf'),
                birth=birth
            )

            login(request, user)
            return redirect('home')
    else:
        form = PatientForm()
    return render(request, 'signup.html', {'form': form})

def del_patient(request):
    patient = request.user.patient
    if not patient.can_delete():
        return redirect('home')

    patient.user.delete()
    return redirect('home')

def select_spec(request):
    if request.method == 'POST':
        form = SpecialtyForm(request.POST)
        if form.is_valid():
            spec = form.cleaned_data.get('specialty')
            return redirect('select_date', spec=spec)
    else:
        form = SpecialtyForm()
    return render(request, 'sel_spec.html', {'form': form})

def select_date(request, spec):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            year = str(date.today().year)
            month = form.cleaned_data.get('month')
            day = form.cleaned_data.get('day')

            return redirect('choose_doc', spec=spec, year=year, month=month, day=day)
            # return render(request, 'choose_doc.html', {'form': form})
    else:
        form = DateForm()
    return render(request, 'sel_date.html', {'form': form})

def sched_date(request):
    if request.method == 'POST':
        form = DateForm2(request.POST)
        if form.is_valid():
            schedules = []
            year = int(form.cleaned_data.get('year'))
            month = int(form.cleaned_data.get('month'))
            day = int(form.cleaned_data.get('day'))

            schedule = request.user.doctor.get_schedule(date(year=year, month=month, day=day))
            schedules.append(schedule)

            return render(request, 'home_doc.html', {'schedules': schedules, 'today': date.today(), 'type': 2})
    else:
        form = DateForm2()
    return render(request, 'sel_date2.html', {'form': form})

def choose_doc(request, spec, year, month, day):
    if request.method == 'POST':
        queue = Queue.objects.get(pk=request.POST['idk'])
        Appointment.objects.create(
            patient=request.user.patient,
            schedule=queue.schedule,
            date=queue.schedule.date,
            time=queue.time)
        return redirect('home')
    else:
        scheds = []
        already_appointed = request.user.patient.current_appoint_doc()

        doctors = Doctor.filter_spec(spec)
        date_a = date(year=int(year), month=int(month), day=int(day))

        for doc in doctors:
            if doc.pk not in already_appointed:
                scheds.append(doc.get_schedule(date_a))

        return render(request, 'choose_doc.html', {'schedules': scheds, 'spec': spec})

def del_appointment(request, app_pk):
    appoint = get_object_or_404(Appointment, pk=app_pk)
    if request.user.patient == appoint.patient:
        appoint.cancel()
        return redirect('home')
    return HttpResponse('<h1> Forbidden Operation </h1>')

def start_appointment(request, app_pk):
    appoint = get_object_or_404(Appointment, pk=app_pk)
    if request.method == 'POST':
        form = StartAppointForm(request.POST)
        if form.is_valid():
            appoint.symptoms = form.cleaned_data.get('symptoms')
            appoint.prescription = form.cleaned_data.get('prescription')
            appoint.exams = form.cleaned_data.get('exams')
            appoint.status = 2
            appoint.save()
            return redirect('schedule_appointments', pkey=appoint.schedule.pk)
    else:
        form = StartAppointForm(
            initial={
                'symptoms': appoint.symptoms,
                'prescription': appoint.prescription,
                'exams': appoint.exams
            }
        )
    return render(request, 'start_appoint.html', {'form': form, 'appoint': app_pk})

def view_appointment(request, app_pk):
    appoint = get_object_or_404(Appointment, pk=app_pk)

    return render(request, 'view_appoint.html', {"appointment": appoint})

def rel_patients(request, rel):
    patients = Patient.objects.all()

    q_ids = [o.pk for o in patients if o.can_delete()]

    patients = patients.filter(pk__in=q_ids)

    return render(request, 'rel_patient.html', {'patients': patients, 'rel': rel})

@method_decorator(login_required, name='dispatch')
class UserUpdateView(UpdateView):
    model = User
    fields = ('first_name', 'last_name', 'email',)
    template_name = 'my_account.html'
    success_url = reverse_lazy('my_account')

    def get_object(self):
        return self.request.user

