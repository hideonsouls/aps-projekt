
from datetime import date as dat
from datetime import datetime as dattime

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.exceptions import ValidationError
from django.conf import settings
from appointments.manager import CustomUserManager


class User(AbstractUser):
    USER_TYPE_CHOICES = (
        (1, 'Secretary'),
        (2, 'Doctor'),
        (3, 'Patient'),
    )

    grp = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES)

    objects = CustomUserManager()

    def get_grp(self):
        return self.USER_TYPE_CHOICES[self.grp-1][1]

class Secretary(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True)

class Doctor(models.Model):
    SPECIALTIES_OPT = (
        (1, 'Orthodontist'),
        (2, 'General dentist'),
        (3, 'Radiologist'),
        (4, 'Dermatologist'),
        (5, 'Anesthesiologist'),
        (6, 'Ophthalmologist'),
        (7, 'Pediatrics'),
        (8, 'Psychiatrist'),
        (9, 'Surgeon'),
        (10, 'Cardiologist')
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True)
    specialty = models.PositiveSmallIntegerField(choices=SPECIALTIES_OPT)
    cpf = models.CharField(max_length=11, unique=True, blank=False)

    def get_spec(self):
        return self.SPECIALTIES_OPT[self.specialty-1][1]

    def get_schedule(self, date):
        if self.appointment_times.filter(date=date).exists():
            return self.appointment_times.filter(date=date).first()

        return Schedule.objects.create(doctor=self, date=date)

    def total_appoints(self):
        scheds = self.appointment_times.all()
        total = 0

        for each in scheds:
            total += each.count_appoints()

        return total

    @staticmethod
    def filter_spec(spec):
        return Doctor.objects.filter(specialty=spec).order_by('user__first_name')

class Patient(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True)
    cpf = models.CharField(max_length=11, unique=True, blank=False)
    birth = models.DateField()

    def get_user_appointments(self):
        self.update()
        return Appointment.objects.filter(patient=self).order_by('-date')

    def can_delete(self):
        if self.get_user_appointments().filter(status__in=[1, 2, 4]):
            return False
        return True

    def current_appoint_doc(self):
        arr = Appointment.objects.filter(patient=self, status__in=[1, 4])
        return [i.schedule.doctor.pk for i in arr]

    def get_usr_age(self):
        return int((dat.today() - self.birth).days / 365)

    def update(self):
        arr = Appointment.objects.filter(patient=self).order_by('date')
        for appoint in arr:
            if appoint.date < dat.today():
                if appoint.status == 1:
                    appoint.status = 3
                    appoint.save()
                elif appoint.status == 4:
                    appoint.delete()

class Schedule(models.Model):
    doctor = models.ForeignKey(
        Doctor, related_name='appointment_times', on_delete=models.PROTECT)
    date = models.DateField()

    class Meta:
        unique_together = ('doctor', 'date',)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(Schedule, self).save(*args, **kwargs)

            for ite in range(8, 11):
                Queue.objects.create(schedule=self, time=ite)
        else:
            super(Schedule, self).save(*args, **kwargs)


    def sch_queues(self):
        # return Queue.objects.filter(schedule=self)
        return self.queues.all()

    def sch_queue(self, time):
        # return Queue.objects.filter(schedule=self).get(time=time)
        return self.queues.get(time=time)

    def get_appoints(self):
        result = []
        queues = self.sch_queues()

        self.update()

        for time, objq in enumerate(queues, start=8):
            if objq.queue_first():
                result.append(self.appointments.filter(time=time).get(patient=objq.queue_first()))
            else:
                result.append('')

        return result

    def count_appoints(self):
        return 3 - self.get_appoints().count('')


    def update(self):
        arr = Appointment.objects.filter(schedule=self)
        for appoint in arr:
            if appoint.date < dat.today():
                if appoint.status == 1:
                    appoint.status = 3
                    appoint.save()
                elif appoint.status == 4:
                    appoint.delete()


class Appointment(models.Model):
    TIME_OPT = (
        (8, '8:00h - 9:00h'),
        (9, '9:00h - 10:00h'),
        (10, '10:00h - 11:00h')
    )

    STATUS_OPT = (
        (1, 'Scheduled'),
        (2, 'Done'),
        (3, 'Missed'),
        (4, 'Waiting'),
    )

    patient = models.ForeignKey(
        Patient, related_name='appointments', on_delete=models.PROTECT)
    schedule = models.ForeignKey(
        Schedule, related_name='appointments', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    date = models.DateField(null=True)
    time = models.PositiveSmallIntegerField(choices=TIME_OPT)
    symptoms = models.TextField(
        max_length=2000, blank=True)
    prescription = models.TextField(
        max_length=2000, blank=True)
    exams = models.TextField(
        max_length=2000, blank=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_OPT, default=1)

    def save(self, *args, **kwargs):
        if self.date < dat.today() and settings.DENY_PAST_APPOINTMENTS:
            raise ValidationError("Appointment's date lower than today's date")

        if not self.pk:
            self.date = self.schedule.date

        if self.schedule.sch_queue(time=self.time).queue_srch(self.patient.pk) > -1:
            super(Appointment, self).save(*args, **kwargs)
        elif self.schedule.sch_queue(time=self.time).queue_add(self.patient.pk):
            if self.schedule.sch_queue(time=self.time).queue_first() != str(self.patient.pk):
                self.status = 4
            super(Appointment, self).save(*args, **kwargs)


    def cancel(self):
        queue = self.schedule.sch_queue(time=self.time)
        first = queue.queue_first()
        if self.can_cancel():
            if queue.queue_remove(self.patient.pk):
                queue.save()
                if queue.queue_first() != first and queue.queue_first() != '':
                    first = queue.schedule.appointments.get(patient=queue.queue_first())
                    if first.status != 1:
                        first.status = 1
                        first.save()
                self.delete()
                #TODO Notify next patient on queue
                return True
            return False
        return False

    def status_str(self):
        return self.STATUS_OPT[self.status - 1][1]

    def can_cancel(self):
        today = dattime.today()
        appoint_day = dattime(
            year=self.date.year,
            month=self.date.month,
            day=self.date.day,
            hour=self.time
        )

        diff = appoint_day - today

        if diff.days < 1:
            return False
        return True

    def time_str(self):
        return self.TIME_OPT[self.time - 8][1]

class Queue(models.Model):
    TIME_OPT = (
        (8, '8:00h - 9:00h'),
        (9, '9:00h - 10:00h'),
        (10, '10:00h - 11:00h')
    )

    schedule = models.ForeignKey(
        Schedule, related_name='queues', on_delete=models.PROTECT)
    time = models.PositiveSmallIntegerField(choices=TIME_OPT)
    queue = models.CharField(max_length=200, blank=True, default="")

    def arrayfy(self):
        return self.queue.split(',')[:-1]

    def queue_add(self, pat_pk):
        pos = self.queue_srch(pat_pk=pat_pk)
        if pos < 0:
            result = ""
            arr = self.arrayfy()
            arr.append(str(pat_pk))

            for i in arr:
                result += i + ','

            self.queue = result
            self.save()
            return True
        raise ValidationError('Patient already on queue for this appointment time')


    def queue_remove(self, pat_pk):
        pos = self.queue_srch(pat_pk=pat_pk)
        if pos > -1:
            result = ''
            arr = self.arrayfy()
            arr.pop(pos)

            for i in arr:
                result += i + ','

            self.queue = result
            self.save()
            return True
        return False

    def queue_srch(self, pat_pk):
        arr = self.arrayfy()

        try:
            result = arr.index(str(pat_pk))
        except ValueError:
            return -1
        return result

    def queue_first(self):
        arr = self.arrayfy()
        if not arr:
            return ''
        return arr[0]
