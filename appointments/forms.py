from datetime import date
from django import forms
# from django.forms import SelectDateWidget
# from flatpickr import DatePickerInput
# from django.contrib.admin import widgets
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UserChangeForm
# from django.contrib.auth.models import User
from .models import User, Appointment
SPECIALTIES_OPT = (
    (1, 'Orthodontist'),
    (2, 'General dentist'),
    (3, 'Radiologist'),
    (4, 'Dermatologist'),
    (5, 'Anesthesiologist'),
    (6, 'Ophthalmologist'),
    (7, 'Pediatrics'),
    (8, 'Psychiatrist'),
    (9, 'Surgeon'),
    (10, 'Cardiologist')
)

DAYS = tuple([(n, n) for n in range(1, 32)])

MONTHS = [
    (1, 'January'),
    (2, 'February'),
    (3, 'March'),
    (4, 'April'),
    (5, 'May'),
    (6, 'June'),
    (7, 'July'),
    (8, 'August'),
    (9, 'September'),
    (10, 'October'),
    (11, 'November'),
    (12, 'December')
]

YEARS = [(n, n) for n in range(1900, 2021)]
YEARS = YEARS[::-1]



class SignUpForm(UserCreationForm):
    # email = forms.CharField(max_length=254, required=True, widget=forms.EmailInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'grp')


class AuthForm(AuthenticationForm):
    # email = forms.CharField(max_length=254, required=True, widget=forms.EmailInput())
    class Meta:
        model = User
        fields = ('username', 'password')


class DoctorForm(UserCreationForm):

    specialty = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=SPECIALTIES_OPT,
    )
    cpf = forms.CharField(
        max_length=11, min_length=11, required=True
    )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',
                  'cpf', 'password1', 'password2', 'specialty')

class DoctorUpd(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(DoctorUpd, self).__init__(*args, **kwargs)
        del self.fields['password']

    specialty = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=SPECIALTIES_OPT,
    )
    cpf = forms.CharField(
        max_length=11, min_length=11, required=True
    )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',
                  'cpf', 'specialty')




class PatientForm(UserCreationForm):

    cpf = forms.CharField(
        max_length=11, min_length=11, required=True
    )
    # birth = forms.DateField(widget=DatePickerInput())
    day = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=DAYS,
    )

    month = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=MONTHS,
    )

    year = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=YEARS,
    )

    # test = forms.DateField(
    #     required=True,
    #     widget=SelectDateWidget(years=YEARS, months=MONTHS)
    # )

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',
                  'cpf', 'password1', 'password2', 'day', 'month', 'year')

class SpecialtyForm(forms.Form):
    specialty = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=SPECIALTIES_OPT,
    )

class DateForm(forms.Form):
    day = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=DAYS,
        initial=date.today().day
    )

    month = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=MONTHS,
        initial=date.today().month
    )

class DateForm2(forms.Form):
    day = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=DAYS,
        initial=date.today().day
    )

    month = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=MONTHS,
        initial=date.today().month
    )
    year = forms.ChoiceField(
        required=True,
        widget=forms.Select,
        choices=YEARS,
    )

class StartAppointForm(forms.ModelForm):

    class Meta:
        model = Appointment
        fields = ('symptoms', 'prescription', 'exams')
