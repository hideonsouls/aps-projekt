"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path  # , path
from django.contrib.auth import views as auth_views

from appointments import views

urlpatterns = [
    re_path(r'^$', views.home, name='home'),

    re_path(r'^register/doctor/$',
            views.reg_doctor, name='reg_doctor'),
    re_path(r'^delete/doctor/(?P<doc_pk>\d+)/$',
            views.del_doctor, name='del_doctor'),
    re_path(r'^update/doctor/(?P<doc_pk>\d+)/$',
            views.upd_doctor, name='upd_doctor'),
    re_path(r'^register/patient/$',
            views.reg_patient, name='reg_patient'),
    re_path(r'^delete/account/$',
            views.del_patient, name='del_patient'),

    re_path(r'^schedules/(?P<pkey>\d+)/$',
            views.schedule_appointments, name='schedule_appointments'),

    re_path(r'^schedules/date/$',
            views.sched_date, name='sched_date'),

    re_path(r'^schedules/appointment/(?P<app_pk>\d+)$',
            views.start_appointment, name='start_appointment'),

    re_path(r'^view/appointment/(?P<app_pk>\d+)$',
            views.view_appointment, name='view_appointment'),

    re_path(r'^appointment/spec/$',
            views.select_spec, name='select_spec'),

    re_path(r'^appointment/spec/(?P<spec>\d+)/date/$',
            views.select_date, name='select_date'),

    re_path(r'^appointment/spec/(?P<spec>\d+)/date/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/(?P<day>[0-9]+)$',
            views.choose_doc, name='choose_doc'),

    re_path(r'^appointment/cancel/(?P<app_pk>\d+)/$',
            views.del_appointment, name='del_appointment'),

    re_path(r'^reports/patient/(?P<rel>\d+)/$',
            views.rel_patients, name='rel_patients'),

    re_path(r'^login/$',
            auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),

    re_path(r'^settings/account/$',
            views.UserUpdateView.as_view(), name='my_account'),

    re_path(r'^reset/$',
            auth_views.PasswordResetView.as_view(
                template_name='password_reset.html',
                email_template_name='password_reset_email.html',
                subject_template_name='password_reset_subject.txt'
            ), name='password_reset'),
    re_path(r'^reset/done/$',
            auth_views.PasswordResetDoneView.as_view(
                template_name='password_reset_done.html'),
            name='password_reset_done'),
    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.PasswordResetConfirmView.as_view(
                template_name='password_reset_confirm.html'),
            name='password_reset_confirm'),
    re_path(r'^reset/complete/$',
            auth_views.PasswordResetCompleteView.as_view(
                template_name='password_reset_complete.html'),
            name='password_reset_complete'),

    re_path(r'^settings/password/$', auth_views.PasswordChangeView.as_view(
        template_name='password_change.html'), name='password_change'),
    re_path(r'^settings/password/done$', auth_views.PasswordChangeDoneView.as_view(
        template_name='password_change_done.html'), name='password_change_done'),


    re_path(r'^admin/', admin.site.urls),
]
